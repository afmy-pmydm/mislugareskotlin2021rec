package net.iescierva.dam20_18.lib

object Principal {
    @JvmStatic
    fun main(args: Array<String>) {
        val lugar = Lugar(
            "Escuela Politécnica Superior de Gandía",
            "C/ Paranimf, 1 46730 Gandia (SPAIN)",
            GeoPunto(38.995656, -0.166093),
            "",
            TipoLugar.OTROS,
            962849300,
            "http://www.epsg.upv.es",
            "Uno de los mejores lugares para formarse.",
            3
        )
        println("Lugar $lugar")
    }
}
