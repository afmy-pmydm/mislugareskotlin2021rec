package net.iescierva.dam20_18.lib

import java.util.ArrayList

class LugaresLista : RepositorioLugares {
    protected var listaLugares: MutableList<Lugar?>
    override fun get_element(id: Int): Lugar? {
        return listaLugares[id]
    }

    override fun add(lugar: Lugar?) {
        listaLugares.add(lugar)
    }

    override fun add_blank(): Int {
        val lugar = Lugar()
        listaLugares.add(lugar)
        return listaLugares.size - 1
    }

    override fun delete(id: Int) {
        listaLugares.removeAt(id)
    }

    override fun size(): Int {
        return listaLugares.size
    }

    override fun update_element(id: Int, lugar: Lugar?) {
        listaLugares[id] = lugar
    }

    fun addEjemplos() {
        add(
            Lugar(
                "Escuela Politécnica Superior de Gandía",
                "C/ Paranimf, 1 46730 Gandia (SPAIN)",
                38.995656, -0.166093, "",
                TipoLugar.EDUCACION, 962849300,
                "http://www.epsg.upv.es",
                "Uno de los mejores lugares para formarse.", 3
            )
        )
        add(
            Lugar(
                "Al de siempre",
                "P.Industrial Junto Molí Nou - 46722, Benifla (Valencia)",
                38.925857, -0.190642, "",
                TipoLugar.BAR, 636472405,
                "",
                "No te pierdas el arroz en calabaza.", 3
            )
        )
        add(
            Lugar(
                "androidcurso.com", "ciberespacio",
                0.0, 0.0, "",
                TipoLugar.EDUCACION, 962849300,
                "http://androidcurso.com",
                "Amplia tus conocimientos sobre Android.", 5
            )
        )
        add(
            Lugar(
                "Barranco del Infierno",
                "Vía Verde del río Serpis. Villalonga (Valencia)",
                38.867180, -0.295058, "",
                TipoLugar.NATURALEZA, 0,
                "http://sosegaos.blogspot.com.es/2009/02/lorcha-villalonga-via-" +
                        "verde-del-rio.html", "Espectacular ruta para bici o andar", 4
            )
        )
        add(
            Lugar(
                "La Vital",
                "Avda. de La Vital, 0 46701 Gandia (Valencia)",
                38.9705949, -0.1720092, "",
                TipoLugar.COMPRAS, 962881070,
                "http://www.lavital.es/",
                "El típico centro comercial", 2
            )
        )
    }

    init {
        listaLugares = ArrayList()
        addEjemplos()
    }
}