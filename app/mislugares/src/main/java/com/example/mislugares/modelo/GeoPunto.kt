package com.example.mislugares.modelo

import java.util.*

open class GeoPunto(d: Double, d1: Double) {
    private var latitud = 0.0
    private var longitud = 0.0

    fun distancia(punto: GeoPunto): Double {
        val RADIO_TIERRA = 6371000.0 // en metros
        val dLat = Math.toRadians(latitud - punto.latitud)
        val dLon = Math.toRadians(longitud - punto.longitud)
        val lat1 = Math.toRadians(punto.latitud)
        val lat2 = Math.toRadians(latitud)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                Math.cos(lat1) * Math.cos(lat2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return c * RADIO_TIERRA
    }

    fun GeoPunto(latitud: Double, longitud: Double) {
        this.latitud = latitud
        this.longitud = longitud
    }

    open fun GeoPunto() {
        this(0.0, 0.0)
    }

    private operator fun invoke(d: Double, d1: Double) {

    }

    fun getLatitud(): Double {
        return latitud
    }

    override fun toString(): String {
        return "GeoPunto{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}'
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val geoPunto = o as GeoPunto
        return java.lang.Double.compare(geoPunto.latitud, latitud) == 0 && java.lang.Double.compare(
            geoPunto.longitud,
            longitud
        ) == 0
    }

    override fun hashCode(): Int {
        return Objects.hash(latitud, longitud)
    }

    fun setLatitud(latitud: Double) {
        this.latitud = latitud
    }

    fun getLongitud(): Double {
        return longitud
    }

    fun setLongitud(longitud: Double) {
        this.longitud = longitud
    }

}