package com.example.mislugares.presentacion

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.example.mislugares.R
import com.example.mislugares.casos_uso.CasosUsoLugar
import com.example.mislugares.datos.RepositorioLugares
import com.example.mislugares.modelo.Lugar

class VistaLugarActivity : Activity() {
    val RESULTADO_EDITAR = 1
    private val lugares: RepositorioLugares? = null
    private val usoLugar: CasosUsoLugar? = null
    private val editarLugar: EdicionLugarActivity? = null
    private val pos = 0
    private var lugar: Lugar? = null
    private val id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vista_lugar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.vista_lugar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.accion_compartir -> true
            R.id.accion_llegar -> true
            R.id.accion_editar -> {
                usoLugar!!.editar(pos, RESULTADO_EDITAR)
                true
            }
            R.id.accion_borrar -> {
                usoLugar!!.dialogoBorrar(pos)
                true
            }
            R.id.accion_guardar -> {
                usoLugar!!.guardar(id, lugar)
                super.onOptionsItemSelected(item)
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULTADO_EDITAR) {
//            editarLugar!!.actualizaVistas()
            findViewById<View>(R.id.scrollView1).invalidate()
        }
    }
}