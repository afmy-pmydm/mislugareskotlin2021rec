package com.example.mislugares.presentacion

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.example.mislugares.Aplicacion
import com.example.mislugares.R
import com.example.mislugares.casos_uso.CasosUsoActividades
import com.example.mislugares.casos_uso.CasosUsoLugar
import com.example.mislugares.datos.RepositorioLugares

class MainActivity : AppCompatActivity() {
    private lateinit var bMostrarLugares: Button
    private lateinit var bPreferencias: Button
    private lateinit var bAcercaDe: Button
    private lateinit var bEditarLugares: Button
    private lateinit var bSalir: Button
    private var lugares: RepositorioLugares? = null
    private var usoLugar: CasosUsoLugar? = null
    private var casoActividades: CasosUsoActividades? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bMostrarLugares = findViewById(R.id.MostrarLugares)
        bMostrarLugares.setOnClickListener(View.OnClickListener { usoLugar!!.mostrar(1) })
        bPreferencias = findViewById(R.id.Preferencias)
        bPreferencias.setOnClickListener(View.OnClickListener {
            casoActividades!!.lanzarPreferencias(
                null
            )
        })
        bAcercaDe = findViewById(R.id.AcercaDe)
        bAcercaDe.setOnClickListener(View.OnClickListener { casoActividades!!.lanzarAcercaDe(null) })
        bEditarLugares = findViewById(R.id.EditarLugares)
        bEditarLugares.setOnClickListener(View.OnClickListener {
            casoActividades!!.lanzarEditarLugar(
                null
            )
        })
        bSalir = findViewById(R.id.Salir)
        bSalir.setOnClickListener(View.OnClickListener { finish() })
        casoActividades = CasosUsoActividades(this)
        lugares = (this.application as Aplicacion).getLugares()
        usoLugar = CasosUsoLugar(this, lugares)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return true
        /** true -> el menú ya está visible  */
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_settings) {
            casoActividades!!.lanzarPreferencias(null)
            return true
        } else if (id == R.id.acercaDe) {
            casoActividades!!.lanzarAcercaDe(null)
            return true
        } else if (id == R.id.menu_buscar) {
            lanzarVistaLugar(null)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun lanzarVistaLugar(view: View?) {
        val entrada = EditText(this)
        entrada.setText("0")
        AlertDialog.Builder(this)
            .setTitle("Selección de lugar")
            .setMessage("indica su id:")
            .setView(entrada)
            .setPositiveButton("Ok") { dialog, whichButton ->
                val id = entrada.text.toString().toInt()
                usoLugar!!.mostrar(id)
            }
            .setNegativeButton("Cancelar", null)
            .show()
    }
}