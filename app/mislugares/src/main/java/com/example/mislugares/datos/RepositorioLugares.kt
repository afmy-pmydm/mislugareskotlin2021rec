package com.example.mislugares.datos

import com.example.mislugares.modelo.Lugar

interface RepositorioLugares {
    fun delete(id: Int)

    fun get_element(pos: Int): Lugar?

    fun update_element(id: Int, nuevoLugar: Lugar?)
}