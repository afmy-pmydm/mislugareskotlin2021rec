package com.example.mislugares.modelo

class GeoPuntoAlt(private val altura: Double, latitud: Double, longitud: Double) :
    GeoPunto(latitud, longitud) {
    var geoPunto: GeoPunto? = null
}