package com.example.mislugares.casos_uso

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import com.example.mislugares.datos.RepositorioLugares
import com.example.mislugares.modelo.Lugar
import com.example.mislugares.presentacion.EdicionLugarActivity
import com.example.mislugares.presentacion.MainActivity
import com.example.mislugares.presentacion.VistaLugarActivity

class CasosUsoLugar(mainActivity: MainActivity, lugares: RepositorioLugares?) {
    // OPERACIONES BÁSICAS
    private var actividad: Activity? = null
    private var lugares: RepositorioLugares? = null

    fun CasosUsoLugar(actividad: Activity, lugares: RepositorioLugares) {
        this.actividad = actividad
        this.lugares = lugares
    }

    // OPERACIONES BÁSICAS
    fun mostrar(pos: Int) {
        val i = Intent(actividad, VistaLugarActivity::class.java)
        i.putExtra("pos", pos)
        actividad?.startActivity(i)
    }

    fun dialogoBorrar(id: Int) {
        AlertDialog.Builder(actividad)
            .setTitle("Borrado de lugar")
            .setMessage("¿Estás seguro de que quieres eliminar este lugar?")
            .setPositiveButton(
                "Confirmar"
            ) { dialog, whichButton ->
                borrar(id)
                actividad?.finish()
            }
            .setNegativeButton("Cancelar", null)
            .show()
    }

    fun borrar(id: Int) {
        lugares?.delete(id)
        actividad?.finish()
    }

    fun guardar(id: Int, nuevoLugar: Lugar?) {
        lugares?.update_element(id, nuevoLugar)
    }

    fun editar(pos: Int, codigoSolicitud: Int) {
        val i = Intent(actividad, EdicionLugarActivity::class.java)
        i.putExtra("pos", pos)
        actividad?.startActivityForResult(i, codigoSolicitud)
    }

}