package com.example.mislugares.presentacion

import android.R
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class PreferenciasActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager.beginTransaction()
            .replace(R.id.content, PreferenciasFragment())
            .commit()
    }
}