package com.example.mislugares.casos_uso

import android.app.Activity
import android.content.Intent
import android.preference.PreferenceManager
import android.view.View
import android.widget.Toast
import com.example.mislugares.presentacion.AcercaDeActivity
import com.example.mislugares.presentacion.EdicionLugarActivity
import com.example.mislugares.presentacion.MainActivity
import com.example.mislugares.presentacion.PreferenciasActivity

class CasosUsoActividades(mainActivity: MainActivity) {
    private var actividad: Activity? = null

    fun CasosUsoActividades(actividad: Activity?) {
        this.actividad = actividad
    }

    fun lanzarAcercaDe(view: View?) {
        val i = Intent(actividad, AcercaDeActivity::class.java)
        actividad!!.startActivity(i)
    }

    fun mostrarPreferencias(view: View?) {
        val pref = PreferenceManager.getDefaultSharedPreferences(actividad)
        val s = ("notificaciones: " + pref.getBoolean("notificaciones", true)
                + ", e-mail: " + pref.getString("email", "")
                + ", tiposNotificaciones: " + pref.getString("tiposNotificaciones", "0")
                + ", máximo a mostrar: " + pref.getString("maximo", "?")
                + ", orden: " + pref.getString("orden", ""))
        Toast.makeText(actividad, s, Toast.LENGTH_SHORT).show()
    }

    fun lanzarPreferencias(view: View?) {
        val i = Intent(actividad, PreferenciasActivity::class.java)
        actividad!!.startActivity(i)
    }

    fun lanzarEditarLugar(view: View?) {
        val i = Intent(actividad, EdicionLugarActivity::class.java)
        actividad!!.startActivity(i)
    }
}