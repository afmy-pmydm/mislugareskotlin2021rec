package com.example.mislugares.modelo

class Lugar {

    private var nombre: String? = null
    private var direccion: String? = null
    private var posicion: GeoPunto? = null
    private var tipo: TipoLugar? = null
    private var foto: String? = null
    private var telefono = 0
    private var url: String
    private var comentario: String? = null
    private var fecha: Long = 0
    private var valoracion = 0f

    @JvmOverloads
    constructor(
        nombre: String?,
        direccion: String?,
        latitud: Double = 0.0,
        longitud: Double = 0.0,
        foto: String? = "",
        tipo: TipoLugar? = TipoLugar.OTROS,
        telefono: Int = 0,
        url: String = "",
        comentario: String? = "",
        valoracion: Int = 0
    ) {
        fecha = System.currentTimeMillis()
        posicion = GeoPunto(latitud, longitud)
        this.foto = foto
        this.tipo = tipo
        this.nombre = nombre
        this.direccion = direccion
        this.telefono = telefono
        this.url = url
        this.comentario = comentario
        this.valoracion = valoracion.toFloat()
    }

    //constructor alternativo recibiendo GeoPunto en lugar de latitud y longitud
    constructor(
        nombre: String?,
        direccion: String?,
        p: GeoPunto,
        foto: String?,
        tipo: TipoLugar?,
        telefono: Int,
        url: String,
        comentario: String?,
        valoracion: Int
    ) : this(
        nombre, direccion, p.getLatitud(), p.getLongitud(), foto,
        tipo, telefono, url, comentario, valoracion
    ) {
    }

    constructor(url: String) {
        this.url = url
    }

    companion object {
        fun getTipo(): TipoLugar {
            return getTipo()
        }

        fun getNombre(): String {
            return getNombre()
        }

        fun getDireccion(): String {
            return getDireccion()
        }

        fun getTelefono(): Int {
            return getTelefono()
        }

        fun getUrl(): String {
            return getUrl()
        }

        fun getComentario(): String {
            return getComentario()
        }
    }
}