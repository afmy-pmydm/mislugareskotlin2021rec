package com.example.mislugares.presentacion

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.mislugares.R
import com.example.mislugares.casos_uso.CasosUsoLugar
import com.example.mislugares.datos.RepositorioLugares

import com.example.mislugares.modelo.Lugar
import com.example.mislugares.modelo.TipoLugar

class EdicionLugarActivity : Activity() {

    private val lugares: RepositorioLugares? = null
    private val usoLugar: CasosUsoLugar? = null
    private val pos = 0
    private val lugar: Lugar? = null
    private var nombre: EditText? = null
    private val tipo: Spinner? = null
    private var direccion: EditText? = null
    private var telefono: EditText? = null
    private var url: EditText? = null
    private var comentario: EditText? = null

    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edicion_lugar)

//        Bundle extras = getIntent().getExtras();
//        pos = extras.getInt("pos", 0);
//        lugares = ((Aplicacion) getApplication()).getLugares();
//        usoLugar = new CasosUsoLugar(this, lugares);
//        lugar = lugares.get_element(pos);
//        tipo = findViewById(R.id.tipo);
//        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item, TipoLugar.getNombres());
//        adaptador.setDropDownViewResource(android.R.layout.
//                simple_spinner_dropdown_item);
//        tipo.setAdapter(adaptador);
//        tipo.setSelection(lugar.getTipo().ordinal());
    }

//    @SuppressLint("WrongViewCast")
//    fun actualizaVistas() {
//        nombre = findViewById(R.id.nombre)
//        nombre.setText(lugar.getNombre())
//        direccion = findViewById(R.id.direccion)
//        direccion.setText(lugar.getDireccion())
//        telefono = findViewById(R.id.telefono)
//        telefono.setText(Integer.toString(lugar.getTelefono()))
//        url = findViewById(R.id.url)
//        url.setText(lugar.getUrl())
//        comentario = findViewById(R.id.comentario)
//        comentario.setText(lugar.getComentario())
//    }
}
