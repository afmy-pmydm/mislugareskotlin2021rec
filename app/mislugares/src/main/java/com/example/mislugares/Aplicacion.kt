package com.example.mislugares

import android.app.Application
import com.example.mislugares.datos.LugaresLista
import com.example.mislugares.datos.RepositorioLugares

class Aplicacion : Application() {
    private val lugares: RepositorioLugares = LugaresLista()
    override fun onCreate() {
        super.onCreate()
    }

    fun getLugares(): RepositorioLugares? {
        return lugares
    }


}